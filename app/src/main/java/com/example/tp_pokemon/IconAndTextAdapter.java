package com.example.tp_pokemon;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class IconAndTextAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> valeurs;

    private static LayoutInflater inflater = null;

    //private int image;

    public IconAndTextAdapter(Context context, ArrayList<String> valeurs) {
        this.context = context;
        this.valeurs = valeurs;
        //this.image = R.drawable.ic_lock_lock;

        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.valeurs.size();
    }

    @Override
    public Object getItem(int position) {
        return this.valeurs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View previous = convertView;
        View retour = previous;

        if(retour == null){
            retour = inflater.inflate(R.layout.list_text_image,null);
        }

        //ImageView image = (ImageView) retour.findViewById(R.id.image_custom);
        TextView text = (TextView) retour.findViewById(R.id.textView);
        text.setText(this.valeurs.get(position));
        //image.setImageResource(this.image);

        return retour;
    }
}
