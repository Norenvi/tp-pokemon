package com.example.tp_pokemon;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.graphics.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "test";
    private static final String API_BASE_URL = "https://pokeapi.co/api/v2/";
    private static int valeurNB = 0;
    List<String> listeValeursDansLaListe = new ArrayList<>();
    private List<String> listeIdPokemon = new ArrayList<>();
    PokemonAPIService servicePokemon;
    Retrofit retrofit;
    private TextView textViewJSON; // TextView dans lequel on va insérer le JSON récupéré de l'API

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflaterMenu = getMenuInflater();
        inflaterMenu.inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.test:
                Toast toast=Toast.makeText(getApplicationContext(),"Bonjour !!!",Toast.LENGTH_SHORT);
                toast.show();
                //String uniqueId = UUID.randomUUID().toString();
                //String valeur = ("valeur " + (valeurNB++));
                //listeValeursDansLaListe.add(valeur);
                break;

            case R.id.quitter:
                System.exit(0);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast toast=Toast.makeText(getApplicationContext(),"Bonjour !!!",Toast.LENGTH_SHORT);
        toast.show();
        this.retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.servicePokemon = retrofit.create(PokemonAPIService.class);
        ListView liste = (ListView) findViewById(R.id.idListView);
        IconAndTextAdapter adapter = new IconAndTextAdapter(  this, (ArrayList<String>) listeValeursDansLaListe);
        liste.setAdapter(adapter);
        final Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Fait un appel à la méthode listPokemons
                // >>> https://pokeapi.co/api/v2/pokemon?limit=20&offset=200 (cf resultat en bas de ce fichier)
                Call<Pokemon> appel = servicePokemon.listPokemons();

                // Appel asynchrone (a privilégier)
                appel.enqueue(new Callback<Pokemon>() {
                    @Override
                    public void onResponse(Call<Pokemon> call, Response<Pokemon> response) {
                        if (response.isSuccessful()) {
                            // Récupère le contenu de la réponse
                            Pokemon contenu = response.body();

                            // On peut du coup parcourir les éléments
                            // Affichage du nombre de pokemons
                            int count = contenu.getCount();
                            Toast.makeText(MainActivity.this, "Nb Pokemons total : " + count, Toast.LENGTH_SHORT).show();

                            // Parcourt les pokemons
                            List<Result> listePokemons = contenu.getResults();
                            StringBuffer chaineConstruite = new StringBuffer();
                            Collections.sort(listePokemons);
                            Pattern p = Pattern.compile("\\/\\d+") ;
                            for (Result res : listePokemons) {
                                Matcher m = p.matcher(res.getUrl()) ;
                                m.find() ;
                                String id = m.group().substring(1);
                                listeIdPokemon.add(id) ;
                                chaineConstruite.append(res.getName() + " > " + res.getUrl() + "\n");
                                listeValeursDansLaListe.add(res.getName());
                                adapter.notifyDataSetChanged();
                            }



                        } else {
                            Toast.makeText(MainActivity.this, "Erreur lors de l'appel à l'API :" + response.errorBody(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Pokemon> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Erreur lors de l'appel à l'API :" + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        Button addButton = (Button)findViewById(R.id.idButtonAdd);
        Button deleteButton = (Button)findViewById(R.id.idButtonDelete);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uniqueId = UUID.randomUUID().toString();
                String valeur = ("valeur "+(valeurNB++));
                listeValeursDansLaListe.add(valeur);
                adapter.notifyDataSetChanged();

                // Pour traiter le pb de la liste vide... Cf ci-dessous
                if (!deleteButton.isEnabled()){
                    deleteButton.setEnabled(true);
                }
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // bof bof ....
                /*
                if (listeValeursDansLaListe.size()>0) {
                    listeValeursDansLaListe.remove(0);
                    adapter.notifyDataSetChanged();
                }
                */

                // Il vaudrait mieux désactiver le bouton s'il n'y a plus de valeur
                if (listeValeursDansLaListe.size() > 0) {
                    listeValeursDansLaListe.remove(0);
                    adapter.notifyDataSetChanged();
                    if (listeValeursDansLaListe.size() == 0) {
                        deleteButton.setEnabled(false);
                    }
                }

            }
        });

        liste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Récupère la valeur de l'item à la position sur laquelle on a cliqué
                String valeurItem = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this,MainActivity2.class);
                intent.putExtra("nom", listeValeursDansLaListe.get(position));
                intent.putExtra("ID", listeIdPokemon.get(position));
                startActivity(intent);
            }
        });




    }

}

// Etape #1 - Création de l'API (Endpoint)
// URL de base = https://pokeapi.co/api/v2/  (doit se terminer par /)
interface PokemonAPIService{
    // 1er appel possible
    @GET("pokemon?limit=50&offset=0")
    Call<Pokemon> listPokemons();

    // 2ème appel possible avec paramètre : IDPOKEMON = id
    @GET("pokemon/{IDPOKEMON}")
    Call<JsonElement> getPokemon(@Path("IDPOKEMON") String id);
}