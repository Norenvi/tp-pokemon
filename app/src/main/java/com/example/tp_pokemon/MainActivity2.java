package com.example.tp_pokemon;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.graphics.Color;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity2 extends AppCompatActivity {

    public static final String TAG = "test";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState) ;
        setContentView(R.layout.layoutactivite2) ;
        TextView label = (TextView)findViewById(R.id.textView2) ;
        label.setText((getIntent().getStringExtra("nom")));
        ImageView image = (ImageView)findViewById(R.id.imageView2) ;
        Button clicButton = (Button) findViewById(R.id.buttonRetour);
        DownloadImageTask download = new DownloadImageTask(image) ;
        download.execute("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"+(getIntent().getStringExtra("ID")+".png"));
        clicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }

        });
    }
}

class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    private ImageView bmImage;
    public DownloadImageTask(ImageView bmImage) {
        this.bmImage = bmImage;
    }
    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap bmp = null;
        InputStream inputStream = null ;
        try {
            URL url = new URL(urldisplay);
            HttpsURLConnection connection = (HttpsURLConnection)
                    url.openConnection();
            connection.connect();
            inputStream = connection.getInputStream();
            bmp = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
        } catch (Exception e) {
            Log.e("Error", e.toString());
        }
        return bmp;
    }
    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
        // Modification de la taille
        bmImage.setScaleX(10);
        bmImage.setScaleY(10);
    }
}